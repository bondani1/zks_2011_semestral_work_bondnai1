package zks;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import zks.tasks.ChangeAddress;
import zks.tasks.LogInCorrectly;
import zks.tasks.NavigateToAddressChange;
import zks.tasks.RegisterDifferentCombinations;
import zks.tasks.StartsWithLogin;
import zks.tasks.StartsWithReg;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;
import static org.hamcrest.Matchers.hasItem;
import static zks.questions.RegFailMessage.theRegFailMessage;
import static zks.questions.RegSucMessage.theRegSucMessage;
import static zks.questions.AddressFailMessage.theAddressFailMessage;
import static zks.questions.AddressSucMessage.theADDRESSSucMessage;


@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "Address-output.csv")
public class ParametrizedAddressTest {

    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String postCode;
    boolean isSuc;

    private WebDriver browser;
    Actor nikita = Actor.named("Nikita");
    String ERROR = "";
    String SUCCESS = "";

    @Before
    public void before() throws IOException {
        browser = new DriverFactory().getDriver();
        givenThat(nikita).can(BrowseTheWeb.with(browser));
    }

    //@After
    public void closeBrowser() {
        browser.close();
    }


    public void setFirstName(String firstName) {
        if (firstName.equals("-")) {
            this.firstName = "";
        } else {
            this.firstName = firstName;
        }
    }


    public void setLastName(String lastName) {
        if (lastName.equals("-")) {
            this.lastName = "";
        } else {
            this.lastName = lastName;
        }
    }


    public void setAddress(String address) {
        this.address = address;
    }


    public void setCity(String city) {
        this.city = city;
    }


    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }


    public void setSuc(boolean suc) {
        isSuc = suc;
    }


    @Test
    public void changeDifferentAddresses() {
        givenThat(nikita).wasAbleTo(StartsWithLogin.loginPage());
        givenThat(nikita).attemptsTo(LogInCorrectly.called());
        givenThat(nikita).attemptsTo(NavigateToAddressChange.called());
        when(nikita).attemptsTo(ChangeAddress.called(this.firstName,this.lastName,this.address,this.city,this.postCode));
        if(!this.isSuc){
            then(nikita).should(seeThat(theAddressFailMessage(),hasItem(ERROR)));
        }else{
            then(nikita).should(seeThat(theADDRESSSucMessage(),hasItem(SUCCESS)));
        }
    }
}
