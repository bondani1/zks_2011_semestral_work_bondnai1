package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.RegisterPage.EMAIL_INPUT;
import static zks.pageObjects.RegisterPage.LASTNAME_INPUT;
import static zks.pageObjects.RegisterPage.NAME_INPUT;
import static zks.pageObjects.RegisterPage.PASSWORD2_INPUT;
import static zks.pageObjects.RegisterPage.PASSWORD_INPUT;
import static zks.pageObjects.RegisterPage.REGISTRATION_BUTTON;
import static zks.pageObjects.RegisterPage.TELEPHONE_INPUT;


public class RegisterDifferentCombinations implements Task {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;


    public RegisterDifferentCombinations(String firstName, String lastName, String email, String phone, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
    }

    public static RegisterDifferentCombinations called(String firstName, String lastName, String email, String phone, String password) {
        return Instrumented.instanceOf(RegisterDifferentCombinations.class).withProperties(firstName, lastName, email, phone, password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        Enter.theValue(this.firstName).into(NAME_INPUT);
        Enter.theValue(this.lastName).into(LASTNAME_INPUT);
        Enter.theValue(this.email).into(EMAIL_INPUT);
        Enter.theValue(this.phone).into(TELEPHONE_INPUT);
        Enter.theValue(this.password).into(PASSWORD_INPUT);
        Enter.theValue(this.password).into(PASSWORD2_INPUT);
        System.out.println(REGISTRATION_BUTTON);
        Click.on(REGISTRATION_BUTTON);
    }

}
