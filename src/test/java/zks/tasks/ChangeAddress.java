package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.AccountPage.CHANGE_ADDRESS_BUTTON;
import static zks.pageObjects.AddressPage.ADDRESS_INPUT;
import static zks.pageObjects.AddressPage.CITY_INPUT;
import static zks.pageObjects.AddressPage.LASTNAME_INPUT;
import static zks.pageObjects.AddressPage.NAME_INPUT;
import static zks.pageObjects.AddressPage.POST_INPUT;
import static zks.pageObjects.AddressPage.SUBMIT_BUTTON;


public class ChangeAddress implements Task {

    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String postCode;


    public ChangeAddress(String firstName, String lastName, String address, String city, String postCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.postCode = postCode;
    }

    public static ChangeAddress called(String firstName, String lastName, String address, String city, String postCode) {
        return Instrumented.instanceOf(ChangeAddress.class).withProperties(firstName, lastName, address, city, postCode);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        Enter.theValue(this.firstName).into(NAME_INPUT);
        Enter.theValue(this.lastName).into(LASTNAME_INPUT);
        Enter.theValue(this.address).into(ADDRESS_INPUT);
        Enter.theValue(this.city).into(CITY_INPUT);
        Enter.theValue(this.postCode).into(POST_INPUT);
        Click.on(SUBMIT_BUTTON);
    }
}
