package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static zks.pageObjects.RegisterPage.ACCOUNT_BUTTON;
import static zks.pageObjects.RegisterPage.LOGOUT_BUTTON;


public class LogOut implements Task {

    public static LogOut called() {
        return Instrumented.instanceOf(LogOut.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ACCOUNT_BUTTON)
        );
    }

}
