package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static zks.pageObjects.DesktopsPage.MAC_BUY_BUTTON;
import static zks.pageObjects.LoginPage.EMAIL_INPUT;


public class PurchaseDesktop implements Task {

    public static PurchaseDesktop called() {
        return Instrumented.instanceOf(PurchaseDesktop.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(MAC_BUY_BUTTON)
        );
    }
}
