package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.AccountPage.CHANGE_PASSWORD;
import static zks.pageObjects.ChangePasswordPage.PASSWORD_CHANGE_BUTTON;
import static zks.pageObjects.ChangePasswordPage.PASSWORD_CONFIRM;
import static zks.pageObjects.ChangePasswordPage.PASSWORD_INPUT;


public class ChangePassword implements Task {

    public static ChangePassword called() {
        return Instrumented.instanceOf(ChangePassword.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CHANGE_PASSWORD),
                Enter.theValue("aaaa").into(PASSWORD_INPUT),
                Enter.theValue("aaaa").into(PASSWORD_CONFIRM),
                Click.on(PASSWORD_CHANGE_BUTTON)
        );
    }
}
