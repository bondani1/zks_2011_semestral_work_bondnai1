package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static zks.pageObjects.AccountPage.CHANGE_ADDRESS_BUTTON;
import static zks.pageObjects.AccountPage.DESKTOPS_BUTTON;
import static zks.pageObjects.AddressPage.CHANGE_ADDRESS;


public class NavigateToAddressChange implements Task {

    public static NavigateToAddressChange called() {
        return Instrumented.instanceOf(NavigateToAddressChange.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CHANGE_ADDRESS_BUTTON),
                Click.on(CHANGE_ADDRESS)
        );
    }
}
