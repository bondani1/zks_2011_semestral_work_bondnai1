package zks.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import zks.pageObjects.LoginPage;
import zks.pageObjects.RegisterPage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartsWithReg implements Task {

    RegisterPage registerPage;

    public static StartsWithReg donoteHomePage() {
        return instrumented(StartsWithReg.class);
    }

    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(registerPage)
        );
    }
}