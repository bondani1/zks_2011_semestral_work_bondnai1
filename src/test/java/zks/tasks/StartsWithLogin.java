package zks.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import zks.pageObjects.LoginPage;

import static net.serenitybdd.screenplay.Tasks.instrumented;


public class StartsWithLogin implements Task {

    LoginPage loginPage;

    public static StartsWithLogin loginPage() {
        return instrumented(StartsWithLogin.class);
    }

    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(loginPage)
        );
    }
}
