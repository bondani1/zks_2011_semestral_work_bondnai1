package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static zks.pageObjects.AccountPage.DESKTOPS_BUTTON;
import static zks.pageObjects.AccountPage.MAC_BUTTON;
import static zks.pageObjects.DesktopsPage.MAC_WISH_BUTTON;


public class AddToWishList implements Task {

    public static AddToWishList called() {
        return Instrumented.instanceOf(AddToWishList.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(DESKTOPS_BUTTON),
                Click.on(MAC_BUTTON),
                Click.on(MAC_WISH_BUTTON)
        );
    }
}
