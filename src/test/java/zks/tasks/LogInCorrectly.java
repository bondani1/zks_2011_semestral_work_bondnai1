package zks.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static zks.pageObjects.LoginPage.EMAIL_INPUT;
import static zks.pageObjects.LoginPage.LOGIN_BUTTON;
import static zks.pageObjects.LoginPage.PASSWORD_INPUT;


public class LogInCorrectly implements Task {

    public static LogInCorrectly called() {
        return Instrumented.instanceOf(LogInCorrectly.class).withProperties();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("a@a.aaaa").into(EMAIL_INPUT),
                Enter.theValue("aaaa").into(PASSWORD_INPUT),
                Click.on(LOGIN_BUTTON)
        );
    }
}
