package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.AccountPage.MY_ACCOUNT;
import static zks.pageObjects.RegisterPage.WARNING;


public class LoginSuccess implements Question<List<String>> {

    public static Question<List<String>> theLoginSuccessMessage() {
        return new LoginSuccess();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(MY_ACCOUNT).viewedBy(actor).asList();
    }
}
