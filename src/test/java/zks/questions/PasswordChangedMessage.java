package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.AccountPage.PASSWORD_CHANGED_MESSAGE;


public class PasswordChangedMessage implements Question<List<String>> {

    public static Question<List<String>> theDisplayPasswordChangedMessage() {
        return new PasswordChangedMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(PASSWORD_CHANGED_MESSAGE).viewedBy(actor).asList();
    }
}
