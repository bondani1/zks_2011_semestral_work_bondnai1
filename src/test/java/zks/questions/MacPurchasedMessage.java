package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.AccountPage.MY_ACCOUNT;
import static zks.pageObjects.DesktopsPage.CART;


public class MacPurchasedMessage implements Question<List<String>> {

    public static Question<List<String>> thePurchaseSuccessMessage() {
        return new MacPurchasedMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(CART).viewedBy(actor).asList();
    }
}
