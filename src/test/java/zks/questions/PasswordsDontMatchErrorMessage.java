package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.ChangePasswordPage.PASSWORD_ERROR;


public class PasswordsDontMatchErrorMessage implements Question<List<String>> {

    public static Question<List<String>> theDisplayPasswordErrorMessage() {
        return new PasswordsDontMatchErrorMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(PASSWORD_ERROR).viewedBy(actor).asList();
    }
}
