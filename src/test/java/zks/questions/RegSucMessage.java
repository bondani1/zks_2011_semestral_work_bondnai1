package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.RegisterPage.REG_FAIL;
import static zks.pageObjects.RegisterPage.REG_SUC;


public class RegSucMessage implements Question<List<String>> {

    public static Question<List<String>> theRegSucMessage() {
        return new RegSucMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REG_SUC).viewedBy(actor).asList();
    }
}
