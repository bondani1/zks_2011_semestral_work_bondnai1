package zks.questions;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Text;
import zks.tasks.AddToWishList;

import java.util.List;

import static zks.pageObjects.DesktopsPage.WISH_LIST;


public class WishListAddedMessage implements Question<List<String>> {

    public static Question<List<String>> theWishListAddedMessage() {
        return new WishListAddedMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(WISH_LIST).viewedBy(actor).asList();
    }
}
