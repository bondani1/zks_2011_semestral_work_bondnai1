package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.RegisterPage.REG_SUC;


public class AddressSucMessage implements Question<List<String>> {

    public static Question<List<String>> theADDRESSSucMessage() {
        return new AddressSucMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REG_SUC).viewedBy(actor).asList();
    }
}
