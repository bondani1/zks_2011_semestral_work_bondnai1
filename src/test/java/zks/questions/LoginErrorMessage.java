package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.LoginPage.LOGIN_WARNING;


public class LoginErrorMessage implements Question<List<String>> {
    public static Question<List<String>> theDisplayLoginErrorMessage() {
        return new LoginErrorMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(LOGIN_WARNING).viewedBy(actor).asList();
    }
}
