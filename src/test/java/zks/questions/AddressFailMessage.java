package zks.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static zks.pageObjects.RegisterPage.REG_FAIL;


public class AddressFailMessage implements Question<List<String>> {

    public static Question<List<String>> theAddressFailMessage() {
        return new AddressFailMessage();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(REG_FAIL).viewedBy(actor).asList();
    }
}
