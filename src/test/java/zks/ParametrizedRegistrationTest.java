package zks;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import zks.tasks.RegisterDifferentCombinations;
import zks.tasks.StartsWithReg;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;
import static org.hamcrest.Matchers.hasItem;
import static zks.questions.RegFailMessage.theRegFailMessage;
import static zks.questions.RegSucMessage.theRegSucMessage;


@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "Registration-output.csv")
public class ParametrizedRegistrationTest {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;
    boolean isSuccess;

    private WebDriver browser;
    Actor nikita = Actor.named("Nikita");
    String ERROR = "";
    String SUCCESS = "";

    @Before
    public void before() throws IOException {
        browser = new DriverFactory().getDriver();
        givenThat(nikita).can(BrowseTheWeb.with(browser));
    }

    @After
    public void closeBrowser() {
        browser.close();
    }


    public void setFirstName(String firstName) {
        if (firstName.equals("-")) {
            this.firstName = "";
        } else {
            this.firstName = firstName;
        }
    }


    public void setLastName(String lastName) {
        if (lastName.equals("-")) {
            this.lastName = "";
        } else {
            this.lastName = lastName;
        }
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public void setFail(boolean fail) {
        isSuccess = fail;
    }


    @Test
    public void registerDifferentCombinations() {
        givenThat(nikita).wasAbleTo(StartsWithReg.donoteHomePage());
        when(nikita).attemptsTo(RegisterDifferentCombinations.called(this.firstName,this.lastName,this.email,this.phone,this.password));
        if(this.isSuccess){
            then(nikita).should(seeThat(theRegFailMessage(),hasItem(ERROR)));
        }else{
            then(nikita).should(seeThat(theRegSucMessage(),hasItem(SUCCESS)));
        }
    }
}

