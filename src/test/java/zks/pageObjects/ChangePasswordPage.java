package zks.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class ChangePasswordPage extends PageObject {

    public static final Target PASSWORD_INPUT = Target
            .the("Account button")
            .locatedBy("//input[@name=\"password\"]");

    public static final Target PASSWORD_CONFIRM = Target
            .the("Account button")
            .locatedBy("//input[@name=\"confirm\"]");

    public static final Target PASSWORD_CHANGE_BUTTON = Target
            .the("Account button")
            .locatedBy("//input[@value=\"Continue\"]");

    public static final Target PASSWORD_ERROR = Target
            .the("Account button")
            .locatedBy("//div[@class=\"text-danger\"]");
}
