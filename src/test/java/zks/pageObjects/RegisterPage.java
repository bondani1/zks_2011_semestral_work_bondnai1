package zks.pageObjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://demo.opencart.com/index.php?route=account/register")
public class RegisterPage extends PageObject {

    public static final Target ACCOUNT_BUTTON = Target
            .the("Account button")
            .locatedBy("//a[@title=\"My Account\"]");

    public static final Target LOGOUT_BUTTON = Target
            .the("Logout button")
            .locatedBy("//ul[@class=\"dropdown-menu dropdown-menu-right\"]/li[position() = last()]");

    public static final Target REGISTRATION_BUTTON = Target
            .the("Register button")
            .locatedBy("//input[@value=\"Continue\"]");

    public static final Target WARNING = Target
            .the("Warning")
            .locatedBy("//div[@class=\"alert alert-danger alert-dismissible\"]");

    public static final Target NAME_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"firstname\"]");

    public static final Target LASTNAME_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"lastname\"]");

    public static final Target EMAIL_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"email\"]");

    public static final Target TELEPHONE_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"telephone\"]");

    public static final Target PASSWORD_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"password\"]");

    public static final Target PASSWORD2_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"confirm\"]");

    public static final Target REG_SUC = Target
            .the("Warning")
            .locatedBy("//input[@value = 1]");

    public static final Target REG_FAIL = Target
            .the("Warning")
            .locatedBy("//input[@value = 0]");
}
