package zks.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class AccountPage extends PageObject {

    public static final Target MY_ACCOUNT = Target
            .the("Account button")
            .locatedBy("//h2[text() = 'My Account']");

    public static final Target CHANGE_PASSWORD = Target
            .the("Account button")
            .locatedBy("//a[text() = 'Change your password']");

    public static final Target PASSWORD_CHANGED_MESSAGE = Target
            .the("Account button")
            .locatedBy("//div[@class=\"alert alert-success alert-dismissible\"]");

    public static final Target DESKTOPS_BUTTON = Target
            .the("Account button")
            .locatedBy("//a[text() = 'Desktops']");

    public static final Target MAC_BUTTON = Target
            .the("Account button")
            .locatedBy("//a[text() = 'Mac (1)']");

    public static final Target CHANGE_ADDRESS_BUTTON = Target
            .the("Account button")
            .locatedBy("//a[text() = 'Modify your address book entries']");
}
