package zks.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://demo.opencart.com/index.php?route=product/category&path=20_27")
public class DesktopsPage extends PageObject {

    public static final Target MAC_BUY_BUTTON = Target
            .the("Account button")
            .locatedBy("//button[@onClick=\"cart.add('41', '1');\"]");

    public static final Target CART = Target
            .the("Account button")
            .locatedBy("//div[@id=\"cart\"]");

    public static final Target MAC_WISH_BUTTON = Target
            .the("Account button")
            .locatedBy("//button[@data-original-title=\"Add to Wish List\"]");

    public static final Target WISH_LIST = Target
            .the("Account button")
            .locatedBy("//div[@class=\"alert alert-success alert-dismissible\"]");
}
