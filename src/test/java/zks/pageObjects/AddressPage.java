package zks.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class AddressPage extends PageObject {

    public static final Target NAME_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"firstname\"]]");

    public static final Target LASTNAME_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"lastname\"]]");

    public static final Target ADDRESS_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"address_1\"]]");

    public static final Target CITY_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"city\"]]");

    public static final Target POST_INPUT = Target
            .the("Warning")
            .locatedBy("//input[@name=\"postcode\"]]");

    public static final Target SUBMIT_BUTTON = Target
            .the("Warning")
            .locatedBy("//button[@value=\"Continue\"]]");

    public static final Target CHANGE_ADDRESS = Target
            .the("Warning")
            .locatedBy("//a[text() = 'New Address']");
}
