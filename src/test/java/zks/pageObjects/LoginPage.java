package zks.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://demo.opencart.com/index.php?route=account/login")
public class LoginPage extends PageObject {

    public static final Target EMAIL_INPUT = Target
            .the("Account button")
            .locatedBy("//input[@name=\"email\"]");

    public static final Target LOGIN_BUTTON = Target
            .the("Account button")
            .locatedBy("//input[@value=\"Login\"]");

    public static final Target PASSWORD_INPUT = Target
            .the("Account button")
            .locatedBy("//input[@name=\"password\"]");

    public static final Target LOGIN_WARNING = Target
            .the("Warning")
            .locatedBy("//div[@class=\"alert alert-danger alert-dismissible\"]");

}
