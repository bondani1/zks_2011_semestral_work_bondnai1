package zks;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import zks.tasks.AddToWishList;
import zks.tasks.ChangePassword;
import zks.tasks.ChangePasswordWrongly;
import zks.tasks.CreateEmptyUser;
import zks.tasks.LogInCorrectly;
import zks.tasks.LogInWithWrongPassword;
import zks.tasks.PurchaseDesktop;
import zks.tasks.StartsWithDesktops;
import zks.tasks.StartsWithReg;
import zks.tasks.StartsWithLogin;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;
import static org.hamcrest.Matchers.hasItem;
import static zks.questions.RegistrationErrorMessage.theDisplayRegistrationErrorMessage;
import static zks.questions.LoginErrorMessage.theDisplayLoginErrorMessage;
import static zks.questions.LoginSuccess.theLoginSuccessMessage;
import static zks.questions.PasswordsDontMatchErrorMessage.theDisplayPasswordErrorMessage;
import static zks.questions.PasswordChangedMessage.theDisplayPasswordChangedMessage;
import static zks.questions.WishListAddedMessage.theWishListAddedMessage;
import static zks.questions.MacPurchasedMessage.thePurchaseSuccessMessage;

@RunWith(SerenityRunner.class)
public class MainTest {

    private WebDriver browser;
    Actor nikita  = Actor.named("Nikita");

    @Before
    public void before() throws IOException {
        browser = new DriverFactory().getDriver();
        givenThat(nikita).can(BrowseTheWeb.with(browser));
    }

    @After
    public void closeBrowser() {
        browser.close();
    }

    @Test
    public void signingInWithWrongPasswordShouldFail() {
        String error = "Warning: No match for E-Mail Address and/or Password.";
        givenThat(nikita).wasAbleTo(StartsWithLogin.loginPage());
        when(nikita).attemptsTo(LogInWithWrongPassword.called());
        then(nikita).should(seeThat(theDisplayLoginErrorMessage(),hasItem(error)));
    }

    @Test
    public void signingInCorrectlyShouldSucceed() {
        String text = "My Account";
        givenThat(nikita).wasAbleTo(StartsWithLogin.loginPage());
        when(nikita).attemptsTo(LogInCorrectly.called());
        then(nikita).should(seeThat(theLoginSuccessMessage(), hasItem(text)));
    }

    @Test
    public void registerWithoutAgreeingPoliciesShouldFail() {
        String error = "Warning: You must agree to the Privacy Policy!";
        givenThat(nikita).wasAbleTo(StartsWithReg.donoteHomePage());
        when(nikita).attemptsTo(CreateEmptyUser.called());
        then(nikita).should(seeThat(theDisplayRegistrationErrorMessage(),hasItem(error)));
    }

    @Test
    public void passwordChangeShouldFailIfPasswordsDontMatch() {
        String error = "Password confirmation does not match password!";
        givenThat(nikita).wasAbleTo(StartsWithLogin.loginPage());
        givenThat(nikita).wasAbleTo(LogInCorrectly.called());
        when(nikita).attemptsTo(ChangePasswordWrongly.called());
        then(nikita).should(seeThat(theDisplayPasswordErrorMessage(), hasItem(error)));
    }

    @Test
    public void passwordChangeShouldSucceed() {
        String text = "Success: Your password has been successfully updated.";
        givenThat(nikita).wasAbleTo(StartsWithLogin.loginPage());
        givenThat(nikita).wasAbleTo(LogInCorrectly.called());
        when(nikita).attemptsTo(ChangePassword.called());
        then(nikita).should(seeThat(theDisplayPasswordChangedMessage(), hasItem(text)));
    }

    @Test
    public void itemPurchaseAddsItToCart() throws InterruptedException {
        String text = "1 item(s) - $122.00";
        givenThat(nikita).wasAbleTo(StartsWithDesktops.desktopsPage());
        when(nikita).attemptsTo(PurchaseDesktop.called());
        Thread.sleep(1000);
        then(nikita).should(seeThat(thePurchaseSuccessMessage(), hasItem(text)));
    }

    @Test
    public void addToWishListShouldSuccees() throws InterruptedException {
        String text = "Success: You have added iMac to your wish list!\n×";
        givenThat(nikita).wasAbleTo(StartsWithLogin.loginPage());
        givenThat(nikita).wasAbleTo(LogInCorrectly.called());
        when(nikita).attemptsTo(AddToWishList.called());
        Thread.sleep(1000);
        then(nikita).should(seeThat(theWishListAddedMessage(), hasItem(text)));
    }
}
